# ULB - How To Make (almost) Any Experiments / FabZero inside

We live in a very challenging time of rapid change and uncertainties. The United Nations have made a list of [17 Sustainable Development Goals](https://sdgs.un.org/) that we, as a world society should urgently act on in the next decade to make a more equitable and sustainable society.

There are many strategies that we can adopt and many ways our society can evolve. Rob Hopkins [^1] who has founded the transition town and Riel Miller [^2] at the UNESCO urge us to **dream, imagine, design and build the future we desire**.

In this class, you will take foot as a **social inventor and entrepreneur** working in **a team**. You will join an interdisciplinary community to **tackle a challenge you care about**.

![](./img/fablab-machine-logos.svg)

We will work at the crossroads of disciplines and community movements:

* **the digital fabrication revolution, the rise of fablabs and the maker movement**. In this class, we learn to use generic tools and workflows that are common in Fab  Labs connected to a wide interdisciplinary community and network. This allows us to develop and design projects globally and collaboratively and to fabricate locally.

* the **Frugal Science movement** which goal is to solve planetary scales problems using cost-effective scientifically based solutions that are scalable to meet the problem scale. You will learn the importance of basic science, tinkering and creative play to tackle design challenges.

* **the growth of creative, practicing and learning communities**, stimulated by **collective intelligence**, able to adapt, collaborate and solve problems. This class will mix undergrad students from different background that will team up with global collaborators and mentors all around the world to solve the identified challenges.

## Class scenario

In this class, you will start by **identifying a set of problems that you are passionate about** as an individual and as a team.

You will **learn organizational and managerial skills in collective intelligence** to solve problems, in interdisciplinary teams, that are bigger than you.

You will **learn to use digital fabrication tools** that you can find in a makerspace or a Fab Lab to build experiments or scientific tools.

Supported by mentors, in teams, you will **design, fabricate and document** a scientifically-based and frugal proof of concept of a project to solve the problem that you have identified as a team.

## Learning Community

### ULB students

[Nikita AKSAKOW](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/nikita.aksakow)  
[Emma BEUEL](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/emma.beuel)  
[Christopher BILBA](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/christopher.bilba)  
[Diane BILGISCHER](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/diane.bilgischer)  
[Armand BOBELEA](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/armand.bobelea)  
[Emilie CELLIER](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/emilie.cellier)  
[Juliette DEBURE](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/juliette.debure)  
[Julien DESUTER](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/julien.desuter)  
[Oubayda EDLEBY](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/oubayda.edleby)  
[Eliott FONTAINE](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/eliott.fontaine)  
[Théo GRIJSPEERDT](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/theo.grijspeerdt)  
[Louis JONAS](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/louis.jonas)  
[Camille LAMON](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/camille.lamon)  
[Noa LIPMANOWICZ](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/noa.lipmanowicz)  
[Jacob MAYORGA MONTAGUANO](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/jacob.mayorga)  
[Anna MOLL](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/anna.moll)  
[Timo PAMBOU](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/timo.pambou)  
[Paloma Del Carmen RECIO FERNANDEZ](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/paloma.reciofernandez)  
[Ana Patrícia RODRIGUES FERNANDES](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/anapatricia.fernandes)  
[Lukas SCHIEBLE](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/lukas.schieble)  
[Quentin STEVENS](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/)  
[Gala TOSCANO-AGUILAR](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/gala.toscano)  
[Edwin VAN LANDUYT](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/edwin.vanlanduyt)  
[Moïra VANDERSLAGMOLEN](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/moira.vanderslagmolen)  
[Ze-Xuan XU](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/zexuan.xu)  
[Mohamed OUAMAR](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/mohamed.ouamar)  
[Gauthier GAILLET](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/gauthier.gaillet)  
[Milan BONTRIDDER](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/milan.bontridder)  
[Alishba Naqi ud din](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/alishba.naqi)  
[Soufiane AJOUAOU](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/soufiane.ajouaou)  
[Ali Bahja](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/ali.bahja)  
[Eva Pueyo ALbo](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/eva.pueyoalbo)  
[Charles Ackermans](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/charles.ackermans)  
[Merlin Pierre](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/merlin.pierre)  
[Eva Dubar](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/eva.dubar) 


### FabZero-Experiments Teaching Designers and Coordinators

* Denis Terwagne, professor of physics and digital fabrication ([Fab Academy](http://archive.fabacademy.org/archives/2017/woma/students/238/), [Frugal Lab](http://frugal.ulb.be/), [FabLab ULB](http://fablab-ulb.be/))

with the help of :  

* Chloé Crokart, community facilitator  ([Collectiv-a](https://collectiv-a.be))
* Sophie Lecloux, pedagogical advisor ([CAP, ULB](https://www.ulb.be/fr/l-ulb-et-l-ecole/cap-centre-d-appui-pedagogique))

### FabZero Mentors

Fab Lab ULB is a community of researchers, teachers, technicians, students and citizens from all disciplines from all disciplines always ready to **volunteer** and **share** their knowledge and **mentor** students. We would like to thank :

* Nicolas De Coster, [IRM meteo](https://www.meteo.be/fr/bruxelles) scientist, scientific collaborator at [FabLab ULB](http://fablab-ulb.be/) ([Fab Academy](http://fab.academany.org/2018/labs/fablabulb/students/nicolas-decoster/))
* Axel Cornu, electronician at [Frugal Lab](https://frugal.ulb.be/) and [FabLab ULB](http://fablab-ulb.be/) ([Fab Academy](https://fabacademy.org/2019/labs/ulb/students/axel-cornu/about/index.html))
* Jonathan Vigne, expert repairer ([Repair Together](https://repairtogether.be/en/what-is-a-repair-cafe/))
* Christophe Reyntiens, technician at EPB
* Guillaume Deneyer, technician at EPB

## Archives

* [2022-2023 ULB FabZero-Experiments Class Website](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/)
* [2021-2022 ULB FabZero-Experiments Class Website](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/class-website/)
* [2020-2021 ULB FabZero-Experiments Class Website](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/)

## References

 [^1]: *From What Is to What If: Unleashing the Power of Imagination to Create the Future We Want*, Chelsea Green Publishing Co, 2019
 [^2]: Resilience Frontiers. Riel Miller. A futures Literacy Laboratory @UNESCO ([video](https://www.youtube.com/watch?v=_WgvTfR7TLI))
