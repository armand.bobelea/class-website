# Computer Controlled Machining

## Goal of this unit

In this unit, you will

* understand what is a CNC milling machine
* learn how to use the CAM tool in Fusion 360 (setting-up and simulation)
* learn to operate a CNC milling machine

## Before class

* Subscribe and install [Fusion 360](https://www.autodesk.com/education/edu-software/overview?sorting=featured&filters=individual)


## Class materials

* TBD

## Assignment

Day 1

* Summarize the safety rules to be followed when using the CNC milling machine.
* Summarize important steps that you need to set up the CNC milling machine and to mill a part
* Assist to a demo

Day 2

* Using a CAD tool, design a part that you would like to mill using the CNC. The part should take no longer than 15 minutes to be milled.
* Prepare and simulate milling your part using a CAM tool.
* Set up the CNC milling machine tool.
* Mill your part !
* Using text, screenshots and photos show how you did the job.
* Show a hero shot of your milled part !

