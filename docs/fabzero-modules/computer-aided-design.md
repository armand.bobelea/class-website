# 2. Computer-Aided Design (CAD)

## Goal of this unit

In this unit, you will learn to

* Learn about compliant mechanism and flexures
* Evaluate and select 3D software
* Demonstrate and describe processes used in modeling with 3D software
* Learn about code licenses and how to use them.
* build on other people's code and give proper credits.

## Before class

Install 2D and 3D design CAD tools :

* [Inkscape](https://inkscape.org/fr/)
* [OpenSCAD](https://openscad.org/)
* [FreeCAD](https://www.freecadweb.org/)

Watch :

* [this video by Veritasium to learn more about what is a Compliant Mechanism](https://www.youtube.com/watch?v=97t7Xj_iBv0)

## Class materials

* [Compliant Mechanisms & flexures](https://gitlab.com/fablab-ulb/enseignements/fabzero/compliant-mechanisms-and-flexures)
* [CAD class tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad)

* Licensing you work
    * [Creative Commons License](https://creativecommons.org/about/cclicenses/)
    * Other licenses :
        * [Free software](https://www.gnu.org/philosophy/free-sw.html), [licences](https://www.gnu.org/licenses/license-list#SoftwareLicenses)
        * [OSI Approved Licenses](https://opensource.org/licenses/)
    * [Writing code guidelines](https://integrity.mit.edu/handbook/writing-code)
    * Licensed code example ([code header](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields/-/blob/master/PFC-Headband-Light-3DPrint/anti_projection.scad), [LICENSE.md](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields/-/blob/master/PFC-Headband-Light-3DPrint/LICENSE.md)) from the [protecive face shield project @ FabLab ULB during the 2020 covid crisis](https://fablab-ulb.gitlab.io/projects/coronavirus/protective-face-shields/).

### Class podcasts

* 2022-2023 - Podcast: CAD
    * [2D vector image (Inkscape) [50min30s]](https://ezplayer.ulb.ac.be/ezplayer/index.php?action=view_asset_details&album=PHYS-F-517-pub&asset=2023_02_23_10h29&asset_token=YPBGQWFZ)
    * [3D CAD (OpenSCAD) [1h16min55s]](https://ezplayer.ulb.ac.be/ezplayer/index.php?action=view_asset_details&album=PHYS-F-517-pub&asset=2023_02_23_10h29&asset_token=YPBGQWFZ)
    * [3D CAD (FreeCAD) [0s]](https://ezplayer.ulb.ac.be/ezplayer/index.php?action=view_asset_details&album=PHYS-F-517-pub&asset=2023_02_23_14h13&asset_token=TQAXWEAY)

## Assignment

* Design, model, and document a parametric [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks) construction kit that you will fabricate next week using 3D printers.
* Make it parametric, so you will be able to make adjustments accounting for the material properties and the machine characteristics.
* Read about [Creative Commons open-source Licence](https://creativecommons.org/about/cclicenses/) and add a CC license to your work.
* Complete your FlexLinks kit that you made by fetching the code of at least 1 part made by another classmate during this week. Acknowledge its work. Modify its code and get the parts ready to print.


