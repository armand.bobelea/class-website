# 3D printing

## Goal of this unit

In this unit, you will learn to

* Identify the advantages and limitations of 3D printing.
* Apply design methods and production processes using 3D printing.

## Before class

* Install the [slicing software](https://en.wikipedia.org/wiki/Slicer_(3D_printing)) : [PrusaSlicer](https://help.prusa3d.com/en/article/install-prusaslicer_1903) on your computer.
* Follow [this 3D printing tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) and prepare the gcode to print the 3D part you designed last week.

## Class materials

* 3D printing
   * [torture test](https://www.thingiverse.com/thing:2806295)  
   * [Design rules]( https://help.prusa3d.com/en/article/modeling-with-3d-printing-in-mind_164135)  
   * [Materials](https://blog.prusaprinters.org/advanced-filament-guide_39718/)  
   * [CNC Kitchen YouTube channel](https://www.youtube.com/@CNCKitchen)

## Assignment

* In group, test the design rules, comment on the choice of materials and good practices for your 3D printer(s) regarding the [kit parts](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/computer-aided-design/) you are going to make.
* In group, test and set the parameters of your flexible part so they can be combined with other classmate parts.
* Individually, 3D print your flexible construction kit you made.
* In group, make a compliant mechanism out of it with several hinges that do something.