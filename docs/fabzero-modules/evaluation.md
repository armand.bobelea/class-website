# Evaluation

## Group live evaluation

In class !

## Auto-evaluation

- For each module, there is a specific evaluation issue that you have to fill.
- For module 1, 2 and 3, download [the  modules 1,2,3 auto-evaluation issues](./../files/Evaluation-Students-Issues-Mod-1-2-3.csv)
- [Import the downloaded csv issues file in your gitlab project](https://docs.gitlab.com/ee/user/project/issues/csv_import.html#import-the-file)
- For each issue, add 1 of the 4 labels that are listed below and assign them to yourself.
    - There are 4 group labels :
        - Assignment not started (red)
        - Assignment in progress(orange)
        - Assignment done (green)
        - Assignment reviewed (blue)
- Each time you start or finish a module, evaluate it and change the label to its current state.
- (optional) You can create an [issue board](https://docs.gitlab.com/ee/user/project/issue_board.html) to ease the tracking of your assignments.

### Auto-evaluation issues for module 4 and 5

- For module 4 and 5, here are the auto-evaluation issues available for download:
    - [Assignment 4: Computer Controlled Cutting](./../files/Evaluation-Students-Issues-Mod-4-CCC.csv)
    - [Assignment 4: Computer Controlled Machining](./../files/Evaluation-Students-Issues-Mod-4-CCM.csv)
    - [Assignment 4: Electronic Prototyping](./../files/Evaluation-Students-Issues-Mod-4-EP.csv)
    - [Assignment 4: 3D Printing for Repair](./../files/Evaluation-Students-Issues-Mod-4-3DP4R.csv)
    - [Assignment 5: Team dynamics and final project](./../files/Evaluation-Students-Issues-Mod-5.csv)

<!-- ## Cross-evaluation

1. Once an evaluation issue of your support pair is labeled as "green", you can evaluate it.
1. To evaluate it copy/paste and edit the "Cross-evaluation canevas" (here below) in the comment section of the module issue you are evaluating.
1. The student who has received feedback from their partner can improve their documentation and respond through the *issues*.
1. Once the review process is done, the evaluated student should change the label from "green-done" to "blue-reviewed"

Examples from last years:

* [cross evaluation for a particular module](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/issues/6)

### Cross-evaluation canevas

```
## Cross-evaluation form - (Write here your evaluator name)

1: strongly disagree  
5: strongly agree

Is the documentation for this module:

**Structured** [1-2-3-4-5]

* Is the structure understood at first glance (quick navigation)?
* Are the purpose, accomplishment and process clearly presented and structured (using headings, code images, sections,...)?

(Comments)

**Complete** [1-2-3-4-5]

* Are the exercise objectives and checklist completed?
* Is the documentation complete? Do the hyperlinks work? Are the files and source codes accessible? Are the machine parameters and references of the materials used provided?

(Comments)

**Honest** [1-2-3-4-5]

* Are sources and inspirations properly referenced? Are the licenses used correctly?
* Is it clear what works, what doesn't work and what needs to be improved?

(Comments)

**pedagogical/appropriate** \[1-2-3-4-5\]

* Based on the documentation, is the work reproducible if one has access to a fablab?
* Is there a pedagogical effort to make the documentation accessible and appropriate (style, clarity of diagrams, comments in the codes,...) ?

(Comments)

(general comment)
``` -->